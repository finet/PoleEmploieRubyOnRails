class OffresController < ApplicationController
  load_and_authorize_resource
  before_action :set_offre, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]
 
  def index
    @offres = Offre.all
  end
 
  def show
  end
 
  def new
    @offre = Offre.new
  end
 
  def edit
  end
 
  def create
    @offre = Offre.new(offre_params)
    @offre.user = current_user
    if @offre.save
      redirect_to @offre, notice: 'Une offre a été crée avec succès.'
    else
      render :new
    end
  end
 
  def update
    if @offre.update(offre_params)
      redirect_to @offre, notice: 'Une offre a été mise à jour avec succès.'
    else
      render :edit
    end
  end
 
  def destroy
    @offre.destroy
    redirect_to offres_url, notice: 'Une offre a été supprimée avec succès.'
  end
 
  private
  def set_offre
    @offre = Offre.find(params[:id])
  end
 
  def offre_params
    params.require(:offre).permit(:name, :job, :descr, :salary)
  end
end
