class Ability
  include CanCan::Ability
  def initialize(user)
    user ||= User.new

    if user.applicant? # demandeur ICI AJOUTER METHODE "postuler" APRÈS "read"
      can [:show, :read], Offre, user_id: user.id

    elsif user.employer? # offreur
      can :manage, Offre
      
    elsif user.admin?
      can :manage, :all

    end
  end
end