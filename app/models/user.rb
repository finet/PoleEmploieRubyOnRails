class User < ActiveRecord::Base
  rolify
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :offres, dependent: :destroy
  
	 
  def applicant?
    has_role?(:applicant)
  end 

  def admin?
    has_role?(:admin)
  end

  def employer?
    has_role?(:employer)
  end 
end
