class CreateOffres < ActiveRecord::Migration[5.1]
  def change
    create_table :offres do |t|
      t.string :name
      t.string :job
      t.string :descr
      t.float :salary

      t.timestamps
    end
  end
end
