class AddUserToOffres < ActiveRecord::Migration[5.1]
  def change
    add_reference :offres, :user, foreign_key: true
  end
end
